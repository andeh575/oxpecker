# Oxpecker

[![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![license: MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/andeh575/oxpecker/-/blob/master/LICENSE)

The [Oxpecker](https://en.wikipedia.org/wiki/Oxpecker) is a real bird. This is
also a real [discord](https://discord.com/) bot that was created while running
across the map in *Life is Feudal* (which is not a relevant detail past
expressing the point that I **obviously** had time).

## Initial Setup

Install `pip`, `pipx`, and `pipenv`:

```sh
# Example is for Ubuntu/Debian

sudo apt-get install python3-pip
python3 -m pip install --user pipx

# Add the following line to your shell configuration file
export PATH=$PATH:~/.local/bin

# Install packages with `pipx` to isolate it from the system python
pipx install pipenv
pipx install black
```

Install packages into the environment:

```sh
pipenv install
```

## Development

Start the bot with the following:

```sh
pipenv run oxpecker.py
```
