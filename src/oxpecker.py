"""
The oxpecker discord bot. Because.
"""
from discord.ext import commands
import discord
import random
import inspect


bot = commands.Bot(command_prefix="$", description="The Oxpecker.")


@bot.event
async def on_ready():
    """
    Print status on successful connection to Discord
    :return: None
    """
    print("Logged in as {}:{}".format(bot.user.name, bot.user.id))


@bot.command()
async def hello():
    """
    Basic greeting
    :return: None
    """
    greetings = ["Hallo", "Hi", "'Sup", "Ugh...", "Howdy", "Greetings"]
    emojis = [":grimacing:", ":smiley:", ":poop:", ":slight_frown:", ":wink:"]

    await bot.say("{0} {1}".format(random.choice(greetings), random.choice(emojis)))


@bot.command(pass_context=True)
async def roll(ctx):
    """
    Roll a d100
    :return: None
    """
    await bot.say(
        "{} rolled {}!".format(ctx.message.author.mention, random.randint(1, 100))
    )


@bot.command()
async def leave():
    """
    Tell the bot to gtfo
    :return: None
    """
    await bot.say("Peace!")
    bot.close()


def load_token():
    """
    Loads a token file for connection to Discordi
    :return: A token string
    :rtype: str
    """
    with open(".token", "r") as f:
        token_string = f.readline().strip()

    return token_string


bot.run(load_token())
